import pandas as pd


import sys, getopt

from log_functions import *

experiment = str(sys.argv[1])

# make paths
experiment_path = "/home/tiago/experiments/" + experiment +"/"
technique_path = experiment_path + "ILP/"
global_routing_path = experiment_path + "cugr/"
detailed_routing_path = experiment_path + "tritonRoute/"
report_path = experiment_path + "report/"

# tail
technique_tail = ".ILP.log"
gr_tail = "_cugr.log"
dr_tail = "_TritonRoute.log"
report_tail = "_eval.score.rpt"

circuit_names = [ "ispd18_test1",
                  "ispd18_test2",
                  "ispd18_test3",
                  "ispd18_test4",
                  "ispd18_test5",
                  "ispd18_test6",
                  "ispd18_test7",
                  "ispd18_test8",
                  "ispd18_test9",
                  "ispd18_test10",
                  "ispd18_test10_noFill",
                ]

# table_columns=[	'movements','percentage',
#                 'gr_wire','gr_vias','gr_runtime',
#                 'dr_wire','dr_vias','dr_runtime',
#                 'score_wire','score_vias','score']

table_columns=[	'movements','percentage',
                'gr_wire','gr_vias','gr_runtime',
                'dr_wire','dr_vias','dr_runtime',
                'score_wire', 'score_wire_g','score_vias','score',
                'out_of_guide_wire', 'out_of_guide_wire_g', 'out_of_guide_vias',
                'off_track_wire', 'off_track_wire_g', 'off_track_vias',
                'wrong_way_wire', 'wrong_way_wire_g',
                'shorts', 'min_area', 'spacing', 'open']

df = pd.DataFrame(index=pd.Index(circuit_names), columns=table_columns)


for circuit in circuit_names:
    ilp_file = technique_path + circuit + technique_tail
    # ilp_file = technique_path + circuit + ".log" #for experiment expT2
    movements = extract_ophidian(ilp_file)
    df["movements"][circuit] = movements

    cugr_file = global_routing_path + circuit + gr_tail
    gr_wirelength, gr_vias, gr_score, gr_runtime = extract_cugr(cugr_file)
#     print("GR wirelength = ", gr_wirelength)
#     print("GR vias = ", gr_vias)
#     print("GR score = ", gr_score)
#     print("GR runtime = ", gr_runtime)
    df["gr_wire"][circuit] = gr_wirelength
    df["gr_vias"][circuit] = gr_vias
    df["gr_runtime"][circuit] = gr_runtime
    
    
    tritonRoute_file = detailed_routing_path + circuit + dr_tail
    dr_wirelength, dr_vias, dr_runtime = extract_tritonRoute(tritonRoute_file)
#     print("DR wirelength = ", dr_wirelength)
#     print("DR vias = ", dr_vias)
    df["dr_wire"][circuit] = dr_wirelength
    df["dr_vias"][circuit] = dr_vias
    df["dr_runtime"][circuit] = dr_runtime
    
    report_file = report_path + circuit + report_tail
#     eval_wirelength, eval_vias, eval_score = extract_eval(report_file)
#     print("Eval wirelength = ", eval_wirelength)
#     print("Eval vias = ", eval_vias)
#     print("Eval score = ", eval_score)
#     df["score_wire"][circuit] = eval_wirelength
#     df["score_vias"][circuit] = eval_vias
#     df["score"][circuit] = eval_score
    
    eval_results = extract_eval_full(report_file)
    df["score_wire"][circuit] = eval_results["wirelength"]
    df["score_wire_g"][circuit] = eval_results["wirelength_g"]
    df["score_vias"][circuit] = eval_results["vias"]
    df["score"][circuit] = eval_results["score"]
    df["out_of_guide_wire"][circuit] = eval_results["out_guide_wire"]
    df["out_of_guide_wire_g"][circuit] = eval_results["out_guide_wire_g"]
    df["out_of_guide_vias"][circuit] = eval_results["out_guide_via"]
    df["off_track_wire"][circuit] = eval_results["off_track_wire"]
    df["off_track_wire_g"][circuit] = eval_results["off_track_wire_g"]
    df["off_track_vias"][circuit] = eval_results["off_track_via"]
    df["wrong_way_wire"][circuit] = eval_results["wrong_way_wire"]
    df["wrong_way_wire_g"][circuit] = eval_results["wrong_way_wire_g"]
    df["shorts"][circuit] = eval_results["short"]
    df["min_area"][circuit] = eval_results["min_area"]
    df["spacing"][circuit] = eval_results["spacing"]
    df["open"][circuit] = eval_results["open_nets"]
    
print(df)
# df.to_csv(experiment_path+"data.csv", na_rep='')
# df.to_csv(experiment_path+"data_noIndex.csv", header=False, index=False, na_rep='')

df.to_csv(experiment_path+"data_full.csv", na_rep='')
df.to_csv(experiment_path+"data_full_noIndex.csv", header=False, index=False, na_rep='')

