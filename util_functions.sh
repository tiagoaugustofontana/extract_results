ICT_EXPERIMENTS=$ipict:/nfs/atipsfs1/tiagoaugustofontana/experiments/
LOCAL_EXPERIMENTS=/home/tiago/experiments/


# sending files to ICT
send_files_to_ICT() {

	echo "sending files to ICT ... "

	sshpass -f /home/tiago/passwordfileICT ssh -T $ipict "
		mkdir experiments/$EXPERIMENT
		mkdir experiments/$EXPERIMENT/cugr
		mkdir experiments/$EXPERIMENT/tritonRoute
	"
	REMOTE=$ICT_EXPERIMENTS$EXPERIMENT/cugr/
	LOCAL=$LOCAL_EXPERIMENTS$EXPERIMENT/cugr/
	echo sshpass -f /home/tiago/passwordfileICT rsync -avzhe ssh --progress $LOCAL $REMOTE
	sshpass -f /home/tiago/passwordfileICT rsync -avzhe ssh --progress $LOCAL $REMOTE
	REMOTE=$ICT_EXPERIMENTS$EXPERIMENT/tritonRoute/
	LOCAL=$LOCAL_EXPERIMENTS$EXPERIMENT/tritonRoute/
	echo sshpass -f /home/tiago/passwordfileICT rsync -avzhe ssh --progress $LOCAL $REMOTE
	sshpass -f /home/tiago/passwordfileICT rsync -avzhe ssh --progress $LOCAL $REMOTE

	echo "end send files"
}

# evaluate circuits in the ICT machine
evaluate_solution(){
echo "evaluating solution ... "

sshpass -f /home/tiago/passwordfileICT ssh -T $ipict << EOSSH
source setup
cd experiments/ispd18eval/
./eval_exps_ssh.sh $EXPERIMENT "${CIRCUITS[@]}" $GR_TAIL $DR_TAIL
EOSSH

echo "end evaluating solution "
}

# # getting the eval from ICT
getting_files_from_ICT() {
	echo "getting files from ICT ... "

	REMOTE=$ICT_EXPERIMENTS$EXPERIMENT/report/
	LOCAL=$LOCAL_EXPERIMENTS$EXPERIMENT/report/
	sshpass -f /home/tiago/passwordfileICT rsync -avzhe ssh --progress $REMOTE $LOCAL

	echo "end getting files from ICT"
}

# cleaning ICT machine
cleaning_ICT() {
	echo "cleaning files in ICT ... "
	sshpass -f /home/tiago/passwordfileICT ssh -T $ipict "
	rm -rf experiments/$EXPERIMENT
	"
	echo "end cleaning ICT "
}
