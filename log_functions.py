import re
import os.path
def extract_ophidian(file):
    number = re.compile("\d+.?\d*e?\+?\d*")
    movements_pat = re.compile("# movements")

    movements = 0
    if not os.path.isfile(file):
        return movements
    for line in open(file):
        if re.search(movements_pat, line):
            result = line.replace("[", "").replace("]", "")
            movements = number.findall(result)[1] # extract the number
    return movements
    
def extract_cugr(file):
    number = re.compile("\d+.?\d*e?\+?\d*")
    start_pat = re.compile("--- Estimated Scores ---")
    wire_pat = re.compile("wirelength \| \d*")
    vias_pat = re.compile("# vias     \| \d*")
    score_pat = re.compile("total score = \d*")
    start_technique = re.compile("finish reading benchmark")
    end_technique = re.compile("Writing guides to file...")
    
    wirelength = 0;
    vias = 0;
    score = 0;
    find_results = False
    start_technique_time = 0
    end_technique_time = 0
    if not os.path.isfile(file):
        return wirelength, vias, score, 0
    for line in open(file):
        if re.search(start_pat, line):
            find_results = True
        if re.search(wire_pat, line) and find_results:
            result =  line.split("|")
            if(len(result) == 5):
                wirelength = result[1].split()[0]
        if re.search(vias_pat, line) and find_results:
            result =  line.split("|")
            if(len(result) == 5):
                vias = result[1].split()[0]
        if re.search(score_pat, line) and find_results:
            find_results = False
            result =  line.split("=")
            if(len(result) == 2):
                score = result[1].split()[0]
        if re.search(start_technique, line):
            result = line.replace("[", "").replace("]", "")
            start_technique_time = number.findall(result)[0] # extract the number
        if re.search(end_technique, line):
            result = line.replace("[", "").replace("]", "")
            end_technique_time = number.findall(result)[0] # extract the number
 
    total_technique_time = float(end_technique_time) - float(start_technique_time)
    return wirelength, vias, score, total_technique_time

def extract_tritonRoute(file):
    number = re.compile("\d+.?\d*e?\+?\d*")
    start_pat = re.compile("complete detail routing")
    wire_pat = re.compile("total wire length = \d*")
    vias_pat = re.compile("total number of vias = \d*")
    runtime_pat = re.compile("Runtime taken")
    
    wirelength = 0;
    vias = 0;
    find_results = False
    runtime = 0
    if not os.path.isfile(file):
        return wirelength, vias, 0
    for line in open(file):
        if re.search(start_pat, line):
            find_results = True
        if re.search(wire_pat, line) and find_results:
            result =  line.split("=")
            if(len(result) == 2):
                wirelength = result[1].split()[0]
        if re.search(vias_pat, line) and find_results:
            find_results = False
            result =  line.split("=")
            if(len(result) == 2):
                vias = result[1].split()[0]
        if re.search(runtime_pat, line):
            runtime = number.findall(line)[0] # extract the number
    return wirelength, vias, runtime

def extract_tritonRoute_0th_iteration(file):
    number = re.compile("\d+.?\d*e?\+?\d*")
    start_pat = re.compile("start 0th optimization iteration ...")
    end_pat = re.compile("tart 1st optimization iteration ...")

    violations_pat = re.compile("number of violations =")
    total_wirelength_pat = re.compile("total wire length =")
    wirelength_pat = re.compile("total wire length on LAYER")
    vias_pat = re.compile("total number of vias = ")

    results = {}
    results["violations"] = -1;
    results["vias"] = -1;
    results["wirelength"] = -1;

    for x in range(1, 17):
        results["Metal{}".format(x)] = -1

    find_results = False
    if not os.path.isfile(file):
        return results
    for line in open(file):
        if re.search(start_pat, line):
            find_results = True
        if re.search(end_pat, line):
            find_results = False
            break
        if re.search(violations_pat, line) and find_results:
            result =  line.split("=")
            if(len(result) == 2):
                results["violations"] = result[1].split()[0]
        if re.search(total_wirelength_pat, line) and find_results:
            result =  line.split("=")
            if(len(result) == 2):
                results["wirelength"] = result[1].split()[0]
        if re.search(vias_pat, line) and find_results:
            result =  line.split("=")
            if(len(result) == 2):
                results["vias"] = result[1].split()[0]
        if re.search(wirelength_pat, line) and find_results:
            result = line.split(" ")
            if(len(result) == 9):
                results[ result[5] ] = result[7].split()[0]
    return results

def extract_eval(file):
    wire_pat = re.compile("Total wire length \| \d*")
    vias_pat = re.compile("Total via count \| \d*")
    score_pat = re.compile("Total Score \| \d*")
    
    wirelength = 0;
    vias = 0;
    score = 0;
    if not os.path.isfile(file):
        return wirelength, vias, score
    for line in open(file):
        if re.search(wire_pat, line):
            result =  line.split("|")
            if(len(result) == 8):
                wirelength = result[2].split()[0]
        if re.search(vias_pat, line):
            result =  line.split("|")
            if(len(result) == 8):
                vias = result[2].split()[0]
        if re.search(score_pat, line):
            find_results = False
            result =  line.split("|")
            if(len(result) == 5):
                score = result[2].split()[0]
    return wirelength, vias, score

def extract_eval_full(file):
    wire_pat = re.compile("Total wire length \| \d*")
    vias_pat = re.compile("Total via count \| \d*")
    score_pat = re.compile("Total Score \| \d*")
    
    out_guide_wire_pat = re.compile("Out-of-guide wire \| \d*")
    out_guide_via_pat = re.compile("Out-of-guide vias \| \d*")
    off_track_wire_pat = re.compile("Off-track wire \| \d*")
    off_track_via_pat = re.compile("Off-track via \| \d*")
    wrong_way_wire_pat = re.compile("Wrong-way wire \| \d*")
    
    short_pat = re.compile("Area of metal shorts \| \d*")
    min_area_pat = re.compile("#min-area violations \| \d*")
    spacing_pat = re.compile("#spacing violations \| \d*")
    open_nets_pat = re.compile("#open nets \| \d*")
    
    results = {}
    results["wirelength"] = 0;
    results["wirelength_g"] = 0;
    results["vias"] = 0;
    results["score"] = 0;
    results["out_guide_wire"] = 0;
    results["out_guide_wire_g"] = 0;
    results["out_guide_via"] = 0;
    results["off_track_wire"] = 0;
    results["off_track_wire_g"] = 0;
    results["off_track_via"] = 0;
    results["wrong_way_wire"] = 0;
    results["wrong_way_wire_g"] = 0;
    results["short"] = 0;
    results["min_area"] = 0;
    results["spacing"] = 0;
    results["open_nets"] = 0;
    
    if not os.path.isfile(file):
        return results
    for line in open(file):
        # Routing
        if re.search(wire_pat, line):
            result =  line.split("|")
            if(len(result) == 8):
                results["wirelength"]   = result[2].split()[0]
                results["wirelength_g"] = result[3].split()[0]
        if re.search(vias_pat, line):
            result =  line.split("|")
            if(len(result) == 8):
                results["vias"] = result[2].split()[0]
                
        # Guides and tracks Obedience 
        if re.search(out_guide_wire_pat, line):
            result =  line.split("|")
            if(len(result) == 8):
                results["out_guide_wire"]   = result[2].split()[0]
                results["out_guide_wire_g"] = result[3].split()[0]
        if re.search(out_guide_via_pat, line):
            result =  line.split("|")
            if(len(result) == 8):
                results["out_guide_via"] = result[2].split()[0]
        if re.search(off_track_wire_pat, line):
            result =  line.split("|")
            if(len(result) == 8):
                results["off_track_wire"]   = result[2].split()[0]
                results["off_track_wire_g"] = result[3].split()[0]
        if re.search(off_track_via_pat, line):
            result =  line.split("|")
            if(len(result) == 8):
                results["off_track_via"] = result[2].split()[0]
        if re.search(wrong_way_wire_pat, line):
            result =  line.split("|")
            if(len(result) == 8):
                results["wrong_way_wire"]   = result[2].split()[0]
                results["wrong_way_wire_g"] = result[3].split()[0]        
           
        # Design Rule Violations
        if re.search(short_pat, line):
            result =  line.split("|")
            if(len(result) == 8):
                results["short"] = result[2].split()[0]
        if re.search(min_area_pat, line):
            result =  line.split("|")
            if(len(result) == 8):
                results["min_area"] = result[2].split()[0]
        if re.search(spacing_pat, line):
            result =  line.split("|")
            if(len(result) == 8):
                results["spacing"] = result[2].split()[0]
        if re.search(open_nets_pat, line):
            result =  line.split("|")
            if(len(result) == 8):
                results["open_nets"] = result[2].split()[0]

        # Score
        if re.search(score_pat, line):
            find_results = False
            result =  line.split("|")
            if(len(result) == 5):
                results["score"] = result[2].split()[0]
    return results