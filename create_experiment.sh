mkdir $1
mkdir $1/ILP
mkdir $1/cugr
mkdir $1/tritonRoute
cp template/* $1/
cd $1
ln -s /home/tiago/devel/build/ophidian-research_release/experiments/ISVLSI_router .

sed -i "s/EXPERIMENT=expT/EXPERIMENT=${1}/" ./run_all_circuits.sh

cd ..