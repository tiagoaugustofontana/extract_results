import pandas as pd
import sys, getopt

from log_functions import *

experiment = str(sys.argv[1])

# make paths
experiment_path = "/home/tiago/experiments/" + experiment +"/"
technique_path = experiment_path + "ILP/"
global_routing_path = experiment_path + "cugr/"
detailed_routing_path = experiment_path + "tritonRoute/"
report_path = experiment_path + "report/"

# tail
technique_tail = ".ILP.log"
gr_tail = "_cugr.log"
dr_tail = "_TritonRoute.log"
report_tail = "_eval.score.rpt"

circuit_names = [ "ispd18_test1",
                  "ispd18_test2",
                  "ispd18_test3",
                  "ispd18_test4",
                  "ispd18_test5",
                  "ispd18_test6",
                  "ispd18_test7",
                  "ispd18_test8",
                  "ispd18_test9",
                #   "ispd18_test10",
                  "ispd18_test10_noFill",
                ]

# table_columns=[	'movements','percentage',
#                 'gr_wire','gr_vias','gr_runtime',
#                 'dr_wire','dr_vias','dr_runtime',
#                 'score_wire','score_vias','score']

# df = pd.DataFrame(index=pd.Index(circuit_names), columns=table_columns)

for circuit in circuit_names:

    baseline_tritonRoute_file = "/home/tiago/experiments/baselineAcademic/tritonRoute/" + circuit + dr_tail
    baseline_density_result = extract_tritonRoute_0th_iteration(baseline_tritonRoute_file)

    df_base = pd.DataFrame( baseline_density_result.values(), index=pd.Index(baseline_density_result.keys()), columns=["baseline"] )

    tritonRoute_file = detailed_routing_path + circuit + dr_tail
    experiment_density_result = extract_tritonRoute_0th_iteration(tritonRoute_file)
    df_exp = pd.DataFrame( experiment_density_result.values(), index=pd.Index(experiment_density_result.keys()), columns=[experiment] )

    df = pd.concat([df_base, df_exp], axis=1, join="inner")

    total_wire = float(df["baseline"]["wirelength"])
    for x in range(1, 17):
        metal = "Metal{}".format(x)
        wire = float( df["baseline"][metal] )
        if (wire != -1):
            df.loc[metal, "base%"] =  wire / total_wire

    total_wire = float(df[experiment]["wirelength"])
    for x in range(1, 17):
        metal = "Metal{}".format(x)
        wire = float( df[experiment][metal] )
        if (wire != -1):
            df.loc[metal, experiment+"%"] =  wire / total_wire

    # print(df)

    df.to_csv(experiment_path+"analysis/"+circuit+"_DR_density.csv", na_rep='')
    df.to_csv(experiment_path+"analysis/"+circuit+"_DR_density_noIndex.csv", header=False, index=False, na_rep='')